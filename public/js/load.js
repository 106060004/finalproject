var loadState = {
    preload: function(){
        // Add a 'loading...' label on the screen
        var loadingLabel = game.add.text(game.width/2, 300, 'loading...', { font: '60px Dosis', fill: '#ffffff' });
        loadingLabel.anchor.setTo(0.5, 0.5);
        // Display the progress bar
        var progressBar = game.add.sprite(game.width/2, 450, 'progressBar');
        progressBar.anchor.setTo(0.5, 0.5);
        game.load.setPreloadSprite(progressBar);

        //load background
        game.load.image('BattleBackgrounds01', 'assets/backgrounds/BattleBackgrounds_01.png');
        game.load.image('BattleBackgrounds02', 'assets/backgrounds/BattleBackgrounds_02.png');
        game.load.image('BattleBackgrounds03', 'assets/backgrounds/BattleBackgrounds_03.png');
        game.load.image('BattleBackgrounds04', 'assets/backgrounds/BattleBackgrounds_04.png');
        game.load.image('BattleBackgrounds05', 'assets/backgrounds/BattleBackgrounds_05.png');

        //load base
        game.load.image('Base', 'assets/myBase.png');

        //load icons
        game.load.image('CatIcons_Cat', 'assets/icons/CatIcons_Cat.png');
        game.load.image('CatIcons_TankCat', 'assets/icons/CatIcons_TankCat.png');
        game.load.image('CatIcons_AxeCat', 'assets/icons/CatIcons_AxeCat.png');
        game.load.image('CatIcons_BirdCat', 'assets/icons/CatIcons_BirdCat.png');
        game.load.image('CatIcons_CowCat', 'assets/icons/CatIcons_CowCat.png');
        game.load.image('CatIcons_WhaleCat', 'assets/icons/CatIcons_WhaleCat.png');
        game.load.image('CatIcons_GrossCat', 'assets/icons/CatIcons_GrossCat.png');
        game.load.image('CatIcons_LizardCat', 'assets/icons/CatIcons_LizardCat.png');
        game.load.image('CatIcons_TitanCat', 'assets/icons/CatIcons_TitanCat.png');
        game.load.spritesheet('MoneyLevelIcon', 'assets/icons/MoneyLevelIcon.png', 177, 158);

        //load cats
        game.load.spritesheet('Cats_Cat', 'assets/cats/Cats_Cat.png', 46, 64);
        game.load.spritesheet('Cats_TankCat', 'assets/cats/Cats_TankCat.png', 78, 98);
        game.load.spritesheet('Cats_AxeCat', 'assets/cats/Cats_AxeCat.png', 119, 127);
        game.load.spritesheet('Cats_BirdCat', 'assets/cats/Cats_BirdCat.png', 193, 178);
        game.load.spritesheet('Cats_CowCat', 'assets/cats/Cats_CowCat.png', 151, 124);
        game.load.spritesheet('Cats_WhaleCat', 'assets/cats/Cats_WhaleCat.png', 175, 210);
        game.load.spritesheet('Cats_GrossCat', 'assets/cats/Cats_GrossCat.png', 242, 305);
        game.load.spritesheet('Cats_LizardCat', 'assets/cats/Cats_LizardCat.png', 218, 85);
        game.load.spritesheet('Cats_TitanCat', 'assets/cats/Cats_TitanCat.png', 230, 229);

        //load enemies
        game.load.spritesheet('Enemies_Doge', 'assets/enemies/Enemies_Doge.png', 50, 56);
        game.load.spritesheet('Enemies_Snache', 'assets/enemies/Enemies_Snache.png', 76, 65);
        game.load.spritesheet('Enemies_TeacherBear', 'assets/enemies/Enemies_TeacherBear.png', 169, 211);
        game.load.spritesheet('Enemies_ThoseGuys', 'assets/enemies/Enemies_ThoseGuys.png', 69, 47);

        //load die animation sprite
        game.load.spritesheet('Die', 'assets/Die.png', 200, 120);

        //load puase part
        game.load.image('pauseIcon', 'assets/pause/PauseButton.png');
        game.load.image('pauseBoard', 'assets/pause/PauseIconBack.png');
        game.load.image('pauseCancel', 'assets/pause/PauseIconCancel.png');
        game.load.spritesheet('pauseSound', 'assets/pause/PauseIconSound.png', 100, 100);
        game.load.spritesheet('pauseBgm', 'assets/pause/PauseIconBgm.png', 100, 100);
        game.load.image('pauseEscape', 'assets/pause/PauseIconEnd.png');

        //load audio
        game.load.audio('Bgm', ['assets/audio/game_maoudamashii_5_village08.mp3', 'assets/audio/game_maoudamashii_5_village08.ogg']);
        game.load.audio('Upgrade', ['assets/audio/se_maoudamashii_system49.mp3', 'assets/audio/se_maoudamashii_system49.ogg']);
        game.load.audio('Attack', ['assets/audio/se_maoudamashii_battle17.mp3', 'assets/audio/se_maoudamashii_battle17.ogg']);

        //load MenuImg
        game.load.image('MainMenuBackground', 'assets/mainmenu/MainMenuBackground.png');
        game.load.image('MenuStartButton', 'assets/mainmenu/MenuStartButton.png');
        game.load.image('MenuTalentButton', 'assets/mainmenu/MenuTalentButton.png');
        game.load.image('MenuUpgradeButton', 'assets/mainmenu/MenuUpgradeButton.png');
        game.load.image('ReturnButton', 'assets/mainmenu/ReturnButton.png');

        //load upgrade
        game.load.image('Upgrade_background', 'assets/upgrade/background2.png');
        game.load.image('Upgrade_upgrade', 'assets/upgrade/upgrade.png', );
        game.load.image('Upgrade_back', 'assets/upgrade/back.png');
        game.load.image('Upgrade_left', 'assets/upgrade/left.png');
        game.load.image('Upgrade_right', 'assets/upgrade/right.png');
        game.load.image('Upgrade_Cat', 'assets/upgrade/Upgrade_CatIcons_Cat.png');
        game.load.image('Upgrade_TankCat', 'assets/upgrade/Upgrade_CatIcons_TankCat.png');
        game.load.image('Upgrade_AxeCat', 'assets/upgrade/Upgrade_CatIcons_AxeCat.png');
        game.load.image('Upgrade_TitanCat', 'assets/upgrade/Upgrade_CatIcons_TitanCat.png');
        game.load.image('Upgrade_BirdCat', 'assets/upgrade/Upgrade_CatIcons_BirdCat.png');
        game.load.image('Upgrade_CowCat', 'assets/upgrade/Upgrade_CatIcons_CowCat.png');
        game.load.image('Upgrade_WhaleCat', 'assets/upgrade/Upgrade_CatIcons_WhaleCat.png');
        game.load.image('Upgrade_GrossCat', 'assets/upgrade/Upgrade_CatIcons_GrossCat.png');
        game.load.image('Upgrade_LizardCat', 'assets/upgrade/Upgrade_CatIcons_LizardCat.png');

        //load stage
        game.load.image('Stage_background', 'assets/stage/backgnd.png');
        game.load.spritesheet('Stage_cat', 'assets/stage/Cats_Cat.png', 46, 64);
        game.load.image('Stage_left', 'assets/stage/left.png');
        game.load.image('Stage_right', 'assets/stage/right.png');
        game.load.image('Stage_back', 'assets/stage/backarrow.png');
        game.load.image('Stage_stage', 'assets/stage/flag.png');
        game.load.image('Stage_next', 'assets/stage/swords.png');

        //load select
        game.load.image('Select_background', 'assets/select/backgnd.png');
        game.load.image('Select_Cat', 'assets/select/CatIcons_Cat.png');
        game.load.image('Select_TankCat', 'assets/select/CatIcons_TankCat.png');
        game.load.image('Select_AxeCat', 'assets/select/CatIcons_AxeCat.png');
        game.load.image('Select_BirdCat', 'assets/select/CatIcons_BirdCat.png');
        game.load.image('Select_CowCat', 'assets/select/CatIcons_CowCat.png');
        game.load.image('Select_WhaleCat', 'assets/select/CatIcons_WhaleCat.png');
        game.load.image('Select_GrossCat', 'assets/select/CatIcons_GrossCat.png');
        game.load.image('Select_LizardCat', 'assets/select/CatIcons_LizardCat.png');
        game.load.image('Select_TitanCat', 'assets/select/CatIcons_TitanCat.png');
        game.load.image('Select_none', 'assets/select/flag.png');
        game.load.image('Select_select', 'assets/select/correct.png');
        game.load.image('Select_ok', 'assets/select/vote.png');
        game.load.image('Select_back', 'assets/select/backarrow.png');

        game.load.image('pixel_red', 'assets/pixel_red.png');
        game.load.image('pixel_orange', 'assets/pixel_orange.png');

        //load data to client
        firebase.auth().onAuthStateChanged(async function (user) {
            if (user) {
                var txtAccount = "";
                for(var i=0;user.email[i] != '@'; i++){
                    txtAccount+=user.email[i];
                }
                await(userRef = 'usersData/' + txtAccount);
                await(loadData());
            }
            else {
                window.location.replace("signin.html");
            }
        });
    },
    create: function(){
        //Start next state
        game.time.events.add(1000, function(){
            game.state.start('mainMenu');
        }, this);
    },
    loadData: function(){
        firebase.database().ref(userRef + '/catList/Cat').once('value')
        .then(function (snapshot) {
            console.log(userRef);
            available = snapshot.val().available;
            level = snapshot.val().level;
            if(available == 1) {
                catArray.push('Cat');
                catArrayLevel.push(level);
            }
        })
    
        firebase.database().ref(userRef + '/catList/TankCat').once('value')
        .then(function (snapshot) {
            available = snapshot.val().available;
            level = snapshot.val().level;
            if(available == 1) {
                catArray.push('TankCat');
                catArrayLevel.push(level);
            }
        })
    
        firebase.database().ref(userRef + '/catList/AxeCat').once('value')
        .then(function (snapshot) {
            available = snapshot.val().available;
            level = snapshot.val().level;
            if(available == 1) {
                catArray.push('AxeCat');
                catArrayLevel.push(level);
            }
        })
    
        firebase.database().ref(userRef + '/catList/TitanCat').once('value')
        .then(function (snapshot) {
            available = snapshot.val().available;
            level = snapshot.val().level;
            if(available == 1) {
                catArray.push('TitanCat');
                catArrayLevel.push(level);
            }
        })
    
        firebase.database().ref(userRef + '/catList/BirdCat').once('value')
        .then(function (snapshot) {
            available = snapshot.val().available;
            level = snapshot.val().level;
            if(available == 1) {
                catArray.push('BirdCat');
                catArrayLevel.push(level);
            }
        })
    
        firebase.database().ref(userRef + '/catList/CowCat').once('value')
        .then(function (snapshot) {
            available = snapshot.val().available;
            level = snapshot.val().level;
            if(available == 1) {
                catArray.push('CowCat');
                catArrayLevel.push(level);
            }
        })
    
        firebase.database().ref(userRef + '/catList/WhaleCat').once('value')
        .then(function (snapshot) {
            available = snapshot.val().available;
            level = snapshot.val().level;
            if(available == 1) {
                catArray.push('FishCat');
                catArrayLevel.push(level);
            }
        })
    
        firebase.database().ref(userRef + '/catList/GrossCat').once('value')
        .then(function (snapshot) {
            available = snapshot.val().available;
            level = snapshot.val().level;
            if(available == 1) {
                catArray.push('GrossCat');
                catArrayLevel.push(level);
            }
        })
    
        firebase.database().ref(userRef + '/catList/LizardCat').once('value')
        .then(function (snapshot) {
            available = snapshot.val().available;
            level = snapshot.val().level;
            if(available == 1) {
                catArray.push('LizardCat');
                catArrayLevel.push(level);
            }
        })
    
        firebase.database().ref(userRef + '/money').once('value')
        .then(function (snapshot) {
            gold = snapshot.val().money;
        })
    }
}