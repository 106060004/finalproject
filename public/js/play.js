var playState = {
    preload: function(){
        for(var i=0;i<catArray.length;i++){
            catsData[i].ATK = catsDataInit[i].ATK + (catArrayLevel[i]-1)*5;
            catsData[i].HP = catsDataInit[i].HP + (catArrayLevel[i]-1)*3;   
        }
    },
    create: function(){
        //Adjust
        this.background = game.add.tileSprite(0, 0, 2054, 682, stagesData[stage].bg);
        game.world.setBounds(0, 0, 2054, 682);
        game.camera.x = 1027;

        this.myBase = game.add.sprite(1827, 350, 'Base');
        game.physics.arcade.enable(this.myBase);
        this.myBase.anchor.setTo(.5, .5);
        this.myBase.HP = 1500;
        this.myBase.body.immovable = true;
        this.myBase.body.setSize(150, 300, 10, 17);
        this.myBase.HPLabel = game.add.text(1700, 170, this.myBase.HP+'/'+this.myBase.HP , {font: 'bold 20px Dosis', fill: '#ffffff', stroke: "#000000", strokeThickness: 3});

        this.enemyBase = game.add.sprite(227, 350, 'Base');
        game.physics.arcade.enable(this.enemyBase);
        this.enemyBase.anchor.setTo(.5, .5);
        this.enemyBase.scale.x = -1;
        this.enemyBase.HP = 1500;
        this.enemyBase.body.immovable = true;
        this.enemyBase.body.setSize(150, 300, 10, 17);
        this.enemyBase.HPLabel = game.add.text(270, 170, this.enemyBase.HP+'/'+this.enemyBase.HP , {font: 'bold 20px Dosis', fill: '#ffffff', stroke: "#000000", strokeThickness: 3});

        this.CatIcons = game.add.group();
        var icon;
        for(var i=0 ; i<5 ; i++){
            icon = this.CatIcons.create(1277+i*130, 630, 'CatIcons_'+catsData[playSelection[i].number].name, i);
            icon.anchor.setTo(.5, .5);
            icon.scale.setTo(.8, .8);
            icon.inputEnabled = true;
            icon.events.onInputDown.add(this.clickIcon, this);
            icon.costLabel = game.add.text(1305+i*130, 650, catsData[playSelection[i].number].cost.toString(), {font: 'bold 20px Montserrat', fill: '#ffff00', stroke: "#000000", strokeThickness: 6})
        }
        
        this.cursors = game.input.keyboard.createCursorKeys();

        this.Catsgroup = [];
        for(var i=0 ; i<5 ; i++){
            this.Catsgroup[i] = game.add.group();
            this.Catsgroup[i].enableBody = true;
            this.Catsgroup[i].createMultiple(10, 'Cats_'+catsData[playSelection[i].number].name);
            this.Catsgroup[i].callAll('animations.add', 'animations', 'walk', catsData[playSelection[i].number].animations_walk, 6, true);
            this.Catsgroup[i].callAll('animations.add', 'animations', 'attack', catsData[playSelection[i].number].animations_attack, 6, false);
            this.Catsgroup[i].setAll('anchor.x', .5);
            this.Catsgroup[i].setAll('anchor.y', 1);
            this.Catsgroup[i].setAll('scale.x', catsData[playSelection[i].number].scaleX);
            this.Catsgroup[i].setAll('scale.y', catsData[playSelection[i].number].scaleY);
        }

        this.Enemiesgroup = [];
        for(var i=0 ; i<stagesData[stage].type ; i++){
            this.Enemiesgroup[i] = game.add.group();
            this.Enemiesgroup[i].enableBody = true;
            this.Enemiesgroup[i].createMultiple(10, 'Enemies'+stagesData[stage].enemySeleciton[i].name);
            this.Enemiesgroup[i].callAll('animations.add', 'animations', 'walk', enemiesData[stagesData[stage].enemySeleciton[i].number].animations_walk, 6, true);
            this.Enemiesgroup[i].callAll('animations.add', 'animations', 'attack', enemiesData[stagesData[stage].enemySeleciton[i].number].animations_attack, 6, false);
            this.Enemiesgroup[i].setAll('anchor.x', .5);
            this.Enemiesgroup[i].setAll('anchor.y', 1);
            this.Enemiesgroup[i].setAll('scale.x', enemiesData[stagesData[stage].enemySeleciton[i].number].scaleX);
            this.Enemiesgroup[i].setAll('scale.y', enemiesData[stagesData[stage].enemySeleciton[i].number].scaleY);
        }
        game.time.events.loop(stagesData[stage].CD, function(){
            var interval = game.rnd.integerInRange(1000, 5000);
            game.time.events.add(interval, function(){
                var n = game.rnd.integerInRange(0, stagesData[stage].type-1);
                var enemy = this.Enemiesgroup[n].getFirstDead();
                if(!enemy)
                    return;
                enemy.reset(this.enemyBase.x, this.enemyBase.y+160);
                enemy.animations.play('walk');
                enemy.SPEED = enemiesData[stagesData[stage].enemySeleciton[n].number].SPEED;
                enemy.body.velocity.x = enemy.SPEED;
                enemy.ATK = enemiesData[stagesData[stage].enemySeleciton[n].number].ATK;
                enemy.HP = enemiesData[stagesData[stage].enemySeleciton[n].number].HP;
                enemy.AS = enemiesData[stagesData[stage].enemySeleciton[n].number].AS;
                var array = enemiesData[stagesData[stage].enemySeleciton[n].number].setSize;
                enemy.body.setSize(array[0], array[1], array[2], array[3]);
                enemy.animDie = enemiesData[stagesData[stage].enemySeleciton[n].number].animations_die;
                enemy.time = 0;
                enemy.lastOverlapped = 0;
            }, this);
        }, this);

        this.Dies = game.add.group();
        this.Dies.createMultiple(50, 'Die');
        this.Dies.callAll('animations.add', 'animations', 'Died', [0, 1, 2, 3, 4, 5, 6, 7], 6, false);
        this.Dies.setAll('anchor.x', .5);
        this.Dies.setAll('anchor.y', .5);
        this.Dies.setAll('scale.x', .7);
        this.Dies.setAll('scale.y', .7);

        //寫有關錢的部分
        this.wallet = {
            money: 0,
            moneyMax: 100,
            moneyLevel: 1,
            moneyLevelMax: 8,
            time: 0,
        }
        this.wallet.moneyLabel = game.add.text(2000, 30, this.wallet.money+'/'+this.wallet.moneyMax , {font: 'bold 36px Montserrat', fill: '#ffff00', stroke: "#000000", strokeThickness: 6});
        this.wallet.moneyLabel.anchor.setTo(1, 0);
        this.wallet.MoneyLevelIcon = game.add.sprite(1027, 524, 'MoneyLevelIcon');
        this.wallet.MoneyLevelIcon.frame = 0;
        this.wallet.MoneyLevelIcon.animations.add('OK', [1,2], 6, true);
        this.wallet.MoneyLevelIcon.inputEnabled = true;
        this.wallet.MoneyLevelIcon.events.onInputDown.add(function(){
            if(game.paused)
                return;
            if(this.wallet.money >= this.wallet.moneyLevel*50 && this.wallet.moneyLevel < this.wallet.moneyLevelMax){
                if(this.pauseIcon.SoundState == 0)
                    this.Upgrade.play();
                this.wallet.money -= (this.wallet.moneyLevel*50);
                this.wallet.moneyLevel++;
                this.wallet.moneyMax = 100+50*(this.wallet.moneyLevel-1);
                this.wallet.levelLabel.text = 'LEVEL '+this.wallet.moneyLevel;
                if(this.wallet.moneyLevel != this.wallet.moneyLevelMax)
                    this.wallet.levelCostLabel.text = (this.wallet.moneyLevel*50).toString();
                else
                this.wallet.levelCostLabel.text = 'MAX';
            }
        }, this);
        this.wallet.levelLabel = game.add.text(1035, 515, 'LEVEL '+this.wallet.moneyLevel, {font: 'bold 28px Dosis', fill: '#ffffff', stroke: '#000000', strokeThickness: 9});
        this.wallet.levelCostLabel = game.add.text(1050, 650, (this.wallet.moneyLevel*50).toString(), {font: 'bold 25px Montserrat', fill: '#ffff00', stroke: '#000000', strokeThickness: 6});
        game.time.events.loop(10, function(){
            if(this.wallet.money < this.wallet.moneyMax && this.wallet.time < game.time.now){
                this.wallet.time = game.time.now + 100-5*this.wallet.moneyLevel;
                this.wallet.money++;
                this.wallet.moneyLabel.text = this.wallet.money+'/'+this.wallet.moneyMax;
            }
            if(this.wallet.money >= this.wallet.moneyLevel*50){
                this.wallet.MoneyLevelIcon.animations.play('OK');
            }else{
                this.wallet.MoneyLevelIcon.animations.stop('OK');
                this.wallet.MoneyLevelIcon.frame = 0;
            }
        }, this);

        //音樂的部分
        this.Bgm = game.add.audio('Bgm');
        this.Bgm.loopFull();
        this.Bgm.loop = true;
        this.Upgrade = game.add.audio('Upgrade');
        this.Attack = game.add.audio('Attack');

        //暫停的部分
        this.pauseIcon = game.add.sprite(1032, 5, 'pauseIcon');
        this.pauseIcon.SoundState = 0;
        this.pauseIcon.BgmState = 0;
        this.pauseIcon.inputEnabled = true;
        this.pauseIcon.events.onInputDown.add(function(){
            if(game.paused)
                return;
            game.paused = true;
            this.pauseBoard = game.add.sprite(game.camera.x+game.width/2, game.height/2, 'pauseBoard');
            this.pauseBoard.anchor.setTo(.5, .5);
            this.pauseBoard.scale.setTo(.7, .7);

            this.pauseCancel = game.add.sprite(game.camera.x+game.width/2+130, game.height/2-99, 'pauseCancel');
            this.pauseCancel.anchor.setTo(.5, .5);
            this.pauseCancel.scale.setTo(.7, .7);

            this.pauseSound = game.add.sprite(game.camera.x+game.width/2+35, game.height/2+20, 'pauseSound');
            this.pauseSound.frame = this.pauseIcon.SoundState;
            this.pauseSound.anchor.setTo(.5, .5);
            this.pauseSound.scale.setTo(.7, .7);
            this.pauseSound.inputEnabled = true;
            this.pauseSound.events.onInputDown.add(function(){
                if(this.pauseSound.frame == 0){
                    this.pauseSound.frame = 1;
                    this.pauseIcon.SoundState = 1;
                }else{
                    this.pauseSound.frame = 0;
                    this.pauseIcon.SoundState = 0;
                }
            }, this);

            this.pauseBgm = game.add.sprite(game.camera.x+game.width/2-65, game.height/2+20, 'pauseBgm');
            this.pauseBgm.frame = this.pauseIcon.BgmState;
            this.pauseBgm.anchor.setTo(.5, .5);
            this.pauseBgm.scale.setTo(.7, .7);
            this.pauseBgm.inputEnabled = true;
            this.pauseBgm.events.onInputDown.add(function(){
                if(this.pauseBgm.frame == 0){
                    this.pauseBgm.frame = 1;
                    this.pauseIcon.BgmState = 1;
                    this.Bgm.pause();
                }else{
                    this.pauseBgm.frame = 0;
                    this.pauseIcon.BgmState = 0;
                    this.Bgm.play();
                }
            }, this);

            this.pauseEscape = game.add.sprite(game.camera.x+game.width/2-15, game.height/2+100, 'pauseEscape');
            this.pauseEscape.anchor.setTo(.5, .5);
            this.pauseEscape.scale.setTo(.7, .7);
            this.pauseEscape.inputEnabled = true;
            this.pauseEscape.events.onInputDown.add(function(){
                game.paused = false;
                this.Bgm.stop();//!!!!!!!!!!!!!!!!!!!!!!!!!
                game.state.start('mainMenu');
            }, this);

            this.pauseCancel.inputEnabled = true;
            this.pauseCancel.events.onInputDown.add(function(){
                this.pauseBoard.destroy();
                this.pauseCancel.destroy();
                this.pauseSound.destroy();
                this.pauseBgm.destroy();
                this.pauseEscape.destroy();
                game.paused = false;
            }, this);

        }, this);
        
        //結束的部分
        this.emitter = game.add.emitter(0, 0, 1000);
        this.emitter.makeParticles(['pixel_red','pixel_orange']);
        this.emitter.setYSpeed(-150, 1000);
        this.emitter.setXSpeed(-1000, 1000);
        this.emitter.setScale(5, 1, 5, 1, 800);
        this.emitter.gravity = 500;
    },
    update: function(){
        this.moveBackground();

        for(var i=0 ; i<5 ; i++){
            game.physics.arcade.overlap(this.enemyBase, this.Catsgroup[i], this.attackEnemyBase, null, this);//group should be second
        }
        for(var j=0 ; j<stagesData[stage].type ; j++){
            game.physics.arcade.overlap(this.myBase, this.Enemiesgroup[j], this.attackMyBase, null, this);
        }

        for(var i=0 ; i<5 ; i++){
            for(var j=0 ; j<stagesData[stage].type ; j++){
                game.physics.arcade.overlap(this.Enemiesgroup[j], this.Catsgroup[i], this.fight, null, this);
            }
        }
        
        for(var i=0 ; i<5 ; i++){
            this.Catsgroup[i].forEach((cat)=>{
                if (cat.alive && cat.lastOverlapped && game.time.now > cat.lastOverlapped && !cat.animDieisPlaying){
                    cat.body.velocity.x = cat.SPEED;
                    cat.animations.play('walk');
                    cat.time = 0;
                }
            }, this);
        }
        for(var j=0 ; j<stagesData[stage].type ; j++){
            this.Enemiesgroup[j].forEach((enemy)=>{
                if (enemy.alive && enemy.lastOverlapped && game.time.now > enemy.lastOverlapped && !enemy.animDieisPlaying){
                    enemy.body.velocity.x = enemy.SPEED;
                    enemy.animations.play('walk');
                    enemy.time = 0;
                }
            }, this);
        }
    },
    render: function(){
        //game.debug.cameraInfo(game.camera, 32, 32);
        //game.debug.body(this.myBase);
        //game.debug.body(this.pauseCancel);
        /*for(var i=0 ; i<5 ; i++){
            this.Catsgroup[i].forEach((cat)=>{
                if(cat.alive){
                    game.debug.body(cat);
                    game.debug.spriteCoords(cat, 32, 500);
                }
            }, this);
        }*/
        /*for(var i=0 ; i<stagesData[stage].type ; i++){
            this.Enemiesgroup[i].forEach((enemy)=>{
                if(enemy.alive){
                    game.debug.body(enemy);
                    game.debug.spriteCoords(enemy, 32, 500);
                }
            }, this);
        }*/
    },
    moveBackground: function(){
        if(this.cursors.left.isDown && game.camera.x > 0){
            game.camera.x -= 8;
            this.CatIcons.forEach((icon)=>{
                icon.x -= 8;
                icon.costLabel.x -= 8;
            }, this);
            this.wallet.moneyLabel.x -= 8;
            this.wallet.MoneyLevelIcon.x -= 8;
            this.wallet.levelLabel.x -= 8;
            this.wallet.levelCostLabel.x -= 8;
            this.pauseIcon.x -= 8;
        }else if(this.cursors.right.isDown && game.camera.x < 1027){
            game.camera.x += 8;
            this.CatIcons.forEach((icon)=>{
                icon.x += 8;
                icon.costLabel.x += 8;
            }, this);
            this.wallet.moneyLabel.x += 8;
            this.wallet.MoneyLevelIcon.x += 8;
            this.wallet.levelLabel.x += 8;
            this.wallet.levelCostLabel.x += 8;
            this.pauseIcon.x += 8;
        }
    },
    clickIcon: function(icon){
        if(game.paused)
            return;
        var cat = this.Catsgroup[this.CatIcons.getIndex(icon)].getFirstDead();
        if(!cat)
            return;
        if(this.wallet.money < catsData[playSelection[this.CatIcons.getIndex(icon)].number].cost){
            return;
        }else{
            this.wallet.money -= catsData[playSelection[this.CatIcons.getIndex(icon)].number].cost;
        }
        var rnd = game.rnd.integerInRange(-5, 5);
        cat.reset(this.myBase.x, this.myBase.y+160+rnd);
        cat.animations.play('walk');
        cat.SPEED = catsData[playSelection[this.CatIcons.getIndex(icon)].number].SPEED;
        cat.body.velocity.x = cat.SPEED;
        cat.ATK = catsData[playSelection[this.CatIcons.getIndex(icon)].number].ATK;
        cat.HP = catsData[playSelection[this.CatIcons.getIndex(icon)].number].HP;
        cat.totalHP = catsData[playSelection[this.CatIcons.getIndex(icon)].number].HP;
        cat.AS = catsData[playSelection[this.CatIcons.getIndex(icon)].number].AS;
        cat.animDie = catsData[playSelection[this.CatIcons.getIndex(icon)].number].animations_die;
        var array = catsData[playSelection[this.CatIcons.getIndex(icon)].number].setSize;
        cat.body.setSize(array[0], array[1], array[2], array[3]);
        cat.time = 0;
        cat.lastOverlapped = 0;
    },
    attackEnemyBase: function(base, cat){//結束如果沒有打到東西要記得回到走路//好難
                                    //可能可以用touching跟wasTouching//但是touching只true一次
        cat.lastOverlapped = game.time.now + 100;
        if(cat.time < game.time.now){
            cat.time = game.time.now+cat.AS;
            cat.animations.play('attack');
            cat.body.velocity.x = 0;
            cat.events.onAnimationComplete.add(function(){//addOnce
                if(this.pauseIcon.SoundState == 0)
                    this.Attack.play();
                base.HP -= cat.ATK;
                base.HPLabel.text = base.HP+'/1500';
                cat.events.onAnimationComplete.removeAll();
            }, this);
            if(base.HP <= 0){
                base.HP = 1;
                game.camera.shake(0.02);
                this.Bgm.stop();
                gold += ((stage+1)*1000);
                getMoneyKey();
                this.emitter.x = game.camera.x+game.width/2;
                this.emitter.y = 100;
                this.emitter.start(true , 800, null, 1000);
                game.time.events.add(1000, function(){
                    game.state.start('mainMenu');
                }, this);
            }   
        }
    },
    attackMyBase: function(base, enemy){
        enemy.lastOverlapped = game.time.now + 100;
        if(enemy.time < game.time.now){
            enemy.time = game.time.now+enemy.AS;
            enemy.animations.play('attack');
            enemy.body.velocity.x = 0;
            enemy.events.onAnimationComplete.add(function(){//addOnce
                base.HP -= enemy.ATK;
                base.HPLabel.text = base.HP+'/1500';
                enemy.events.onAnimationComplete.removeAll();
            }, this);
            if(base.HP <= 0){
                base.HP = 1;
                game.camera.shake(0.02);
                this.Bgm.stop();
                game.time.events.add(1000, function(){
                    game.state.start('mainMenu');
                }, this);
            }            
        }
    },
    fight: function(enemy, cat){
        cat.lastOverlapped = game.time.now + 100;
        enemy.lastOverlapped = game.time.now + 100;
        //因為他在CD，所以有碰到其他貓但是時間還沒到
        if(cat.time < game.time.now){
            cat.time = game.time.now+cat.AS;
            cat.body.velocity.x = 0;
            cat.animations.play('attack');
            //還是用animation complete比較好
            cat.events.onAnimationComplete.add(function(){//addOnce
                if(this.pauseIcon.SoundState == 0)
                    this.Attack.play();
                enemy.HP -= cat.ATK;
                cat.events.onAnimationComplete.removeAll();
            }, this);
        }
        if(enemy.time < game.time.now){
            enemy.time = game.time.now+enemy.AS;
            enemy.body.velocity.x = 0;
            enemy.animations.play('attack');
            //
            enemy.events.onAnimationComplete.add(function(){
                cat.HP -= enemy.ATK;
                enemy.events.onAnimationComplete.removeAll();
            }, this)
        }
        if(cat.HP <= 0){
            cat.HP = 1;
            //加上死亡時的動畫
            var originalX = cat.x;
            var originalY = cat.y;
            cat.animDieisPlaying = true;
            cat.frame = cat.animDie;
            game.add.tween(cat).to({angle: 30}, 10).start();
            game.add.tween(cat).to({x: originalX+80}, 400).start(); 
            game.add.tween(cat).to({y: originalY-30}, 200).yoyo(true).start().onComplete.addOnce(function(){
                originalX = cat.x;
                originalY = cat.y;
                game.add.tween(cat).to({x: originalX+40}, 200).start(); 
                game.add.tween(cat).to({y: originalY-20}, 100).yoyo(true).start().onComplete.addOnce(function(){
                    cat.animDieisPlaying = false;
                    var Die = this.Dies.getFirstDead();
                    if(!Die)
                        return;
                    Die.reset(cat.x, cat.y);
                    Die.animations.play('Died');
                    game.add.tween(Die).to({y: 100 ,alpha: .1}, 1250).start();
                    Die.events.onAnimationComplete.addOnce(function(){
                        Die.events.onAnimationComplete.removeAll();
                        Die.alpha = 1;
                        Die.kill();
                    }, this)
                    cat.angle = 0;
                    cat.kill();
                }, this);
            }, this);
        }
        if(enemy.HP <= 0){
            enemy.HP = 1;
            //加上死亡時的動畫
            var originalX = enemy.x;
            var originalY = enemy.y;
            enemy.animDieisPlaying = true;
            enemy.frame = enemy.animDie;
            game.add.tween(enemy).to({angle: -30}, 10).start();
            game.add.tween(enemy).to({x: originalX-80}, 400).start(); 
            game.add.tween(enemy).to({y: originalY-30}, 200).yoyo(true).start().onComplete.addOnce(function(){
                originalX = enemy.x;
                originalY = enemy.y;
                game.add.tween(enemy).to({x: originalX-40}, 200).start(); 
                game.add.tween(enemy).to({y: originalY-20}, 100).yoyo(true).start().onComplete.addOnce(function(){
                    enemy.animDieisPlaying = false;
                    var Die = this.Dies.getFirstDead();
                    if(!Die)
                        return;
                    Die.reset(enemy.x, enemy.y);
                    Die.animations.play('Died');
                    game.add.tween(Die).to({y: 100 ,alpha: .1}, 1250).start();
                    Die.events.onAnimationComplete.addOnce(function(){
                        Die.events.onAnimationComplete.removeAll();
                        Die.alpha = 1;
                        Die.kill();
                    }, this)
                    enemy.angle = 0;
                    enemy.kill();
                }, this);
            }, this);
        }
    }
}