var selectState = {
    preload: function(){
        
    },
    create: function(){
        //background
        game.stage.backgroundColor = '#3498db';
        this.background = game.add.image(0, 0, 'Select_background');
        //firebase 
        ///1.catlist -> available(level=-1 if not available)
        ///2.select
        getKey();
        this.select = [false,false,false,false,false,false,false,false,false];
        this.member = 0; //at most 5
        //cat icon
        ///need to solve delay of (catArrayLevelAll) to set image
        if(catArrayLevelAll[0]>=1){
            this.slot0 = game.add.image(game.width/2-300, game.height/2-200, 'Select_Cat');
            this.slot0.anchor.setTo(0.5, 0.5);
            this.slot0.scale.setTo(0.8, 0.8);
            this.slot0.inputEnabled = true;
            this.slot0.events.onInputDown.add(this.select0, this);
        }else{
            this.slot0 = game.add.image(game.width/2-300, game.height/2-200, 'Select_none');
            this.slot0.anchor.setTo(0.5, 0.5);
            this.slot0.scale.setTo(0.2, 0.2);
        }
        if(catArrayLevelAll[1]>=1){
            this.slot1 = game.add.image(game.width/2-100, game.height/2-200, 'Select_TankCat');
            this.slot1.anchor.setTo(0.5, 0.5);
            this.slot1.scale.setTo(0.8, 0.8);
            this.slot1.inputEnabled = true;
            this.slot1.events.onInputDown.add(this.select1, this);
        }else{
            this.slot1 = game.add.image(game.width/2-100, game.height/2-200, 'Select_none');
            this.slot1.anchor.setTo(0.5, 0.5);
            this.slot1.scale.setTo(0.2, 0.2);
        }
        if(catArrayLevelAll[2]>=1){
            this.slot2 = game.add.image(game.width/2+200-100, game.height/2-200, 'Select_AxeCat');
            this.slot2.anchor.setTo(0.5, 0.5);
            this.slot2.scale.setTo(0.8, 0.8);
            this.slot2.inputEnabled = true;
            this.slot2.events.onInputDown.add(this.select2, this);
        }else{
            this.slot2 = game.add.image(game.width/2+200-100, game.height/2-200, 'Select_none');
            this.slot2.anchor.setTo(0.5, 0.5);
            this.slot2.scale.setTo(0.2, 0.2);
        }
        if(catArrayLevelAll[3]>=1){
            this.slot3 = game.add.image(game.width/2-200-100, game.height/2, 'Select_BirdCat');
            this.slot3.anchor.setTo(0.5, 0.5);
            this.slot3.scale.setTo(0.8, 0.8);
            this.slot3.inputEnabled = true;
            this.slot3.events.onInputDown.add(this.select3, this);
        }else{
            this.slot3 = game.add.image(game.width/2-200-100, game.height/2, 'Select_none');
            this.slot3.anchor.setTo(0.5, 0.5);
            this.slot3.scale.setTo(0.2, 0.2);
        }
        if(catArrayLevelAll[4]>=1){
            this.slot4 = game.add.image(game.width/2-100, game.height/2, 'Select_CowCat');
            this.slot4.anchor.setTo(0.5, 0.5);
            this.slot4.scale.setTo(0.8, 0.8);
            this.slot4.inputEnabled = true;
            this.slot4.events.onInputDown.add(this.select4, this);
        }else{
            this.slot4 = game.add.image(game.width/2-100, game.height/2, 'Select_none');
            this.slot4.anchor.setTo(0.5, 0.5);
            this.slot4.scale.setTo(0.2, 0.2);
        }
        if(catArrayLevelAll[5]>=1){
            this.slot5 = game.add.image(game.width/2+200-100, game.height/2, 'Select_WhaleCat');
            this.slot5.anchor.setTo(0.5, 0.5);
            this.slot5.scale.setTo(0.8, 0.8);
            this.slot5.inputEnabled = true;
            this.slot5.events.onInputDown.add(this.select5, this);
        }else{
            this.slot5 = game.add.image(game.width/2+200-100, game.height/2, 'Select_none');
            this.slot5.anchor.setTo(0.5, 0.5);
            this.slot5.scale.setTo(0.2, 0.2);
        }
        if(catArrayLevelAll[6]>=1){
            this.slot6 = game.add.image(game.width/2-200-100, game.height/2+200, 'Select_GrossCat');
            this.slot6.anchor.setTo(0.5, 0.5);
            this.slot6.scale.setTo(0.8, 0.8);
            this.slot6.inputEnabled = true;
            this.slot6.events.onInputDown.add(this.select6, this);
        }else{
            this.slot6 = game.add.image(game.width/2-200-100, game.height/2+200, 'Select_none');
            this.slot6.anchor.setTo(0.5, 0.5);
            this.slot6.scale.setTo(0.2, 0.2);
        }
        if(catArrayLevelAll[7]>=1){
            this.slot7 = game.add.image(game.width/2-100, game.height/2+200, 'Select_LizardCat');
            this.slot7.anchor.setTo(0.5, 0.5);
            this.slot7.scale.setTo(0.8, 0.8);
            this.slot7.inputEnabled = true;
            this.slot7.events.onInputDown.add(this.select7, this);
        }else{
            this.slot7 = game.add.image(game.width/2-100, game.height/2+200, 'Select_none');
            this.slot7.anchor.setTo(0.5, 0.5);
            this.slot7.scale.setTo(0.2, 0.2);
        }
        if(catArrayLevelAll[8]>=1){
            this.slot8 = game.add.image(game.width/2+200-100, game.height/2+200, 'Select_TitanCat');
            this.slot8.anchor.setTo(0.5, 0.5);
            this.slot8.scale.setTo(0.8, 0.8);
            this.slot8.inputEnabled = true;
            this.slot8.events.onInputDown.add(this.select8, this);
        }else{
            this.slot8 = game.add.image(game.width/2+200-100, game.height/2+200, 'Select_none');
            this.slot8.anchor.setTo(0.5, 0.5);
            this.slot8.scale.setTo(0.2, 0.2);
        }
        //select icon
        this.sel0 = game.add.image(this.slot0.x+50, this.slot0.y-40, 'Select_select');
        this.sel0.anchor.setTo(0.5,0.5);
        this.sel0.scale.setTo(0.8,0.8);
        this.sel0.alpha = 0;
        this.sel1 = game.add.image(this.slot1.x+50, this.slot1.y-40, 'Select_select');
        this.sel1.anchor.setTo(0.5,0.5);
        this.sel1.scale.setTo(0.8,0.8);
        this.sel1.alpha = 0;
        this.sel2 = game.add.image(this.slot2.x+50, this.slot2.y-40, 'Select_select');
        this.sel2.anchor.setTo(0.5,0.5);
        this.sel2.scale.setTo(0.8,0.8);
        this.sel2.alpha = 0;
        this.sel3 = game.add.image(this.slot3.x+50, this.slot3.y-40, 'Select_select');
        this.sel3.anchor.setTo(0.5,0.5);
        this.sel3.scale.setTo(0.8,0.8);
        this.sel3.alpha = 0;
        this.sel4 = game.add.image(this.slot4.x+50, this.slot4.y-40, 'Select_select');
        this.sel4.anchor.setTo(0.5,0.5);
        this.sel4.scale.setTo(0.8,0.8);
        this.sel4.alpha = 0;
        this.sel5 = game.add.image(this.slot5.x+50, this.slot5.y-40, 'Select_select');
        this.sel5.anchor.setTo(0.5,0.5);
        this.sel5.scale.setTo(0.8,0.8);
        this.sel5.alpha = 0;
        this.sel6 = game.add.image(this.slot6.x+50, this.slot6.y-40, 'Select_select');
        this.sel6.anchor.setTo(0.5,0.5);
        this.sel6.scale.setTo(0.8,0.8);
        this.sel6.alpha = 0;
        this.sel7 = game.add.image(this.slot7.x+50, this.slot7.y-40, 'Select_select');
        this.sel7.anchor.setTo(0.5,0.5);
        this.sel7.scale.setTo(0.8,0.8);
        this.sel7.alpha = 0;
        this.sel8 = game.add.image(this.slot8.x+50, this.slot8.y-40, 'Select_select');
        this.sel8.anchor.setTo(0.5,0.5);
        this.sel8.scale.setTo(0.8,0.8);
        this.sel8.alpha = 0;
        //go back
        this.backbtn = game.add.image(4, 577, 'ReturnButton');
        this.backbtn.inputEnabled = true;
        this.backbtn.events.onInputDown.add(this.JustBack, this);
        //select btn
        this.selectbtn = game.add.image(game.width-14, game.height-78, 'Select_ok');
        this.selectbtn.anchor.setTo(1,1);
        this.selectbtn.inputEnabled = true;
        this.selectbtn.events.onInputDown.add(this.BackAndUpdate, this);
        //some text
        this.goldLabel = game.add.text(800, 3, '' + gold, {fontWeight: 'bold', font: '50px Agency FB ', fill:'#000000'});
    },
    update: function(){
    },
    select0: function(){
        //console.log('before: '+this.select[0]);
        if(this.member < 5 && this.select[0] == false){
            this.select[0] = true;
            this.member++;
            this.sel0.alpha = 1;
        }else{
            if(this.select[0] == true){this.member--;}
            this.select[0] = false;
            this.sel0.alpha = 0;
        }
        //console.log('after: '+this.select[0],this.member);
    },
    select1: function(){
        if(this.member < 5 && this.select[1] == false){
            this.select[1] = true;
            this.member++;
            this.sel1.alpha = 1;
        }else{
            if(this.select[1] == true){this.member--;}
            this.select[1] = false;
            this.sel1.alpha = 0;
        }
        //console.log('after: '+this.select[1],this.member);
    },
    select2: function(){
        if(this.member < 5 && this.select[2] == false){
            this.select[2] = true;
            this.member++;
            this.sel2.alpha = 1;
        }else{
            if(this.select[2] == true){this.member--;}
            this.select[2] = false;
            this.sel2.alpha = 0;
        }
        //console.log('after: '+this.select[2],this.member);
    },
    select3: function(){
        if(this.member < 5 && this.select[3] == false){
            this.select[3] = true;
            this.member++;
            this.sel3.alpha = 1;
        }else{
            if(this.select[3] == true){this.member--;}
            this.select[3] = false;
            this.sel3.alpha = 0;
        }
        //console.log('after: '+this.select[3],this.member);
    },
    select4: function(){
        if(this.member < 5 && this.select[4] == false){
            this.select[4] = true;
            this.member++;
            this.sel4.alpha = 1;
        }else{
            if(this.select[4] == true){this.member--;}
            this.select[4] = false;
            this.sel4.alpha = 0;
        }
        //console.log('after: '+this.select[4],this.member);
    },
    select5: function(){
        if(this.member < 5 && this.select[5] == false){
            this.select[5] = true;
            this.member++;
            this.sel5.alpha = 1;
        }else{
            if(this.select[5] == true){this.member--;}
            this.select[5] = false;
            this.sel5.alpha = 0;
        }
        //console.log('after: '+this.select[5],this.member);
    },
    select6: function(){
        if(this.member < 5 && this.select[6] == false){
            this.select[6] = true;
            this.member++;
            this.sel6.alpha = 1;
        }else{
            if(this.select[6] == true){
                this.member--;
                this.select[6] = false;
                this.sel6.alpha = 0;
            }
        }
        //console.log('after: '+this.select[6],this.member);
    },
    select7: function(){
        if(this.member < 5 && this.select[7] == false){
            this.select[7] = true;
            this.member++;
            this.sel7.alpha = 1;
        }else{
            if(this.select[7] == true){this.member--;}
            this.select[7] = false;
            this.sel7.alpha = 0;
        }
        //console.log('after: '+this.select[7],this.member);
    },
    select8: function(){
        if(this.member < 5 && this.select[8] == false){
            this.select[8] = true;
            this.member++;
            this.sel8.alpha = 1;
        }else{
            if(this.select[8] == true){this.member--;}
            this.select[8] = false;
            this.sel8.alpha = 0;
        }
        //console.log('after: '+this.select[8],this.member);
    },
    BackAndUpdate: function(){
        var team = [];
        for(var i=0;i<9;i++){
            if(this.select[i]==true){
                team.push({
                    number: i
                });
            }
        }
        if(team.length == 0)
            return;
        while(team.length<5){
            team.push(team[team.length-1]);
        }
        playSelection = team;
        //setSelectPlayer(this.team);
        game.state.start('play');
    },
    JustBack: function(){
        game.state.start('stage');
    }
};