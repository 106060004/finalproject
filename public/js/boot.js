var bootState = {
    preload: function(){
        game.load.image('progressBar', 'assets/progressBar.png');//from index.html path
    },
    create: function(){
        game.state.backgroundColor = '#3498db';
        game.physics.startSystem(Phaser.Physics.ARCADE);
        game.renderer.renderSession.roundPixel = true;
        //Start next state
        game.state.start('load');
    }
};