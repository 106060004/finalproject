var stageState = {
    preload: function(){
        
    },
    create: function(){
        //setting
        game.stage.backgroundColor = '#3498db';
        this.background = game.add.image(0, 0, 'Stage_background');
        game.physics.startSystem(Phaser.Physics.ARCADE);
        this.stage = 1; //get stage in firebase
        //firebase
        getKeytoStage();
        //back btn
        this.backbtn = game.add.image(4 ,577, 'ReturnButton');
        this.backbtn.inputEnabled = true;
        this.backbtn.events.onInputDown.add(this.StateBack, this);
        //start btn
        this.startbtn = game.add.image(game.width-14, game.height-78, 'Stage_next');
        this.startbtn.anchor.setTo(1, 1);
        this.startbtn.inputEnabled = true;
        this.startbtn.events.onInputDown.add(this.StateNext, this);
        //stage
        this.stage1 = game.add.image(game.width/2-70, game.height/2+90, 'Stage_stage');
        this.stage1.anchor.setTo(0.5, 0.5);
        this.stage1.scale.setTo(0.07, 0.07);
        this.stage1.inputEnabled = true;
        this.stage1.events.onInputDown.add(this.movetoStage1, this);
        this.stage1_text = game.add.text(this.stage1.x, this.stage1.y-40, '台灣', {font: '30px Arial', fill:'#000000'});
        this.stage1_text.anchor.setTo(0.5, 0.5);
        this.stage2 = game.add.image(350, game.height - 230, 'Stage_stage');
        this.stage2.anchor.setTo(0.5, 0.5);
        this.stage2.scale.setTo(0.07, 0.07);
        this.stage2.inputEnabled = true;
        this.stage2.events.onInputDown.add(this.movetoStage2, this);
        this.stage2_text = game.add.text(this.stage2.x, this.stage2.y-40, '廣東', {font: '30px Arial', fill:'#000000'});
        this.stage2_text.anchor.setTo(0.5, 0.5);
        this.stage3 = game.add.image(game.width/2-70, game.height/2+180, 'Stage_stage');
        this.stage3.anchor.setTo(0.5, 0.5);
        this.stage3.scale.setTo(0.07, 0.07);
        this.stage3.inputEnabled = true;
        this.stage3.events.onInputDown.add(this.movetoStage3, this);
        this.stage3_text = game.add.text(this.stage3.x, this.stage3.y-40, '菲律賓', {font: '30px Arial', fill:'#000000'});
        this.stage3_text.anchor.setTo(0.5, 0.5);
        this.stage4 = game.add.image(180, game.height - 230, 'Stage_stage');
        this.stage4.anchor.setTo(0.5, 0.5);
        this.stage4.scale.setTo(0.07, 0.07);
        this.stage4.inputEnabled = true;
        this.stage4.events.onInputDown.add(this.movetoStage4, this);
        this.stage4_text = game.add.text(this.stage4.x, this.stage4.y-40, '緬甸', {font: '30px Arial', fill:'#000000'});
        this.stage4_text.anchor.setTo(0.5, 0.5);
        this.stage5 = game.add.image(650, game.height/2 - 80, 'Stage_stage');
        this.stage5.anchor.setTo(0.5, 0.5);
        this.stage5.scale.setTo(0.07, 0.07);
        this.stage5.inputEnabled = true;
        this.stage5.events.onInputDown.add(this.movetoStage5, this);
        this.stage5_text = game.add.text(this.stage5.x, this.stage5.y-40, '日本', {font: '30px Arial', fill:'#000000'});
        this.stage5_text.anchor.setTo(0.5, 0.5);
        this.stage6 = game.add.image(525, game.height/2 - 80, 'Stage_stage');
        this.stage6.anchor.setTo(0.5, 0.5);
        this.stage6.scale.setTo(0.07, 0.07);
        this.stage6.inputEnabled = true;
        this.stage6.events.onInputDown.add(this.movetoStage6, this);
        this.stage6_text = game.add.text(this.stage6.x, this.stage6.y-40, '南韓', {font: '30px Arial', fill:'#000000'});
        this.stage6_text.anchor.setTo(0.5, 0.5);
        this.stage7 = game.add.image(225, game.height/2 - 180, 'Stage_stage');
        this.stage7.anchor.setTo(0.5, 0.5);
        this.stage7.scale.setTo(0.07, 0.07);
        this.stage7.inputEnabled = true;
        this.stage7.events.onInputDown.add(this.movetoStage7, this);
        this.stage7_text = game.add.text(this.stage7.x, this.stage7.y-40, '蒙古', {font: '30px Arial', fill:'#000000'});
        this.stage7_text.anchor.setTo(0.5, 0.5);
        this.stage8 = game.add.image(400, game.height/2 - 120, 'Stage_stage');
        this.stage8.anchor.setTo(0.5, 0.5);
        this.stage8.scale.setTo(0.07, 0.07);
        this.stage8.inputEnabled = true;
        this.stage8.events.onInputDown.add(this.movetoStage8, this);
        this.stage8_text = game.add.text(this.stage8.x, this.stage8.y-40, '北京', {font: '30px Arial', fill:'#000000'});
        this.stage8_text.anchor.setTo(0.5, 0.5);
        this.stage9 = game.add.image(40, game.height/2+90, 'Stage_stage');
        this.stage9.anchor.setTo(0.5, 0.5);
        this.stage9.scale.setTo(0.07, 0.07);
        this.stage9.inputEnabled = true;
        this.stage9.events.onInputDown.add(this.movetoStage9, this);
        this.stage9_text = game.add.text(this.stage9.x, this.stage9.y-40, '印度', {font: '30px Arial', fill:'#000000'});
        this.stage9_text.anchor.setTo(0.5, 0.5);
        this.stage10 = game.add.image(120, game.height/2, 'Stage_stage');
        this.stage10.anchor.setTo(0.5, 0.5);
        this.stage10.scale.setTo(0.07, 0.07);
        this.stage10.inputEnabled = true;
        this.stage10.events.onInputDown.add(this.movetoStage10, this);
        this.stage10_text = game.add.text(this.stage10.x, this.stage10.y-40, '西藏', {font: '30px Arial', fill:'#000000'});
        this.stage10_text.anchor.setTo(0.5, 0.5);
        //cat animation
        this.player = game.add.sprite(this.stage1.x, this.stage1.y, 'Stage_cat');
        this.player.animations.add('move', [0, 1, 2], 8, true);
        this.player.anchor.setTo(0.5, 0.5);
        this.player.scale.setTo(0.7, 0.7);
        this.goldLabel = game.add.text(800, 3, '' + gold, {fontWeight: 'bold', font: '50px Agency FB ', fill:'#000000'});
    },
    update: function(){
        this.player.animations.play('move');
    },
    movetoStage1: function(){
        this.stage = 1;
        game.add.tween(this.player).to({x: this.stage1.x, y:this.stage1.y}, 100).start();
    },
    movetoStage2: function(){
        this.stage = 2;
        game.add.tween(this.player).to({x: this.stage2.x, y:this.stage2.y}, 100).start();
    },
    movetoStage3: function(){
        this.stage = 3;
        game.add.tween(this.player).to({x: this.stage3.x, y:this.stage3.y}, 100).start();
    },
    movetoStage4: function(){
        this.stage = 4;
        game.add.tween(this.player).to({x: this.stage4.x, y:this.stage4.y}, 100).start();
    },
    movetoStage5: function(){
        this.stage = 5;
        game.add.tween(this.player).to({x: this.stage5.x, y:this.stage5.y}, 100).start();
    },
    movetoStage6: function(){
        this.stage = 6;
        game.add.tween(this.player).to({x: this.stage6.x, y:this.stage6.y}, 100).start();
    },
    movetoStage7: function(){
        this.stage = 7;
        game.add.tween(this.player).to({x: this.stage7.x, y:this.stage7.y}, 100).start();
    },
    movetoStage8: function(){
        this.stage = 8;
        game.add.tween(this.player).to({x: this.stage8.x, y:this.stage8.y}, 100).start();
    },
    movetoStage9: function(){
        this.stage = 9;
        game.add.tween(this.player).to({x: this.stage9.x, y:this.stage9.y}, 100).start();
    },
    movetoStage10: function(){
        this.stage = 10;
        game.add.tween(this.player).to({x: this.stage10.x, y:this.stage10.y}, 100).start();
    },
    StateNext: function(){
        //go to next stage
        stage = this.stage-1;
        game.state.start('select');
    },
    StateBack: function(){
        game.state.start('mainMenu');
    }
};