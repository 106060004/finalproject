//Initailize phaser
var game = new Phaser.Game(1027, 682, Phaser.AUTO, 'canvas');//Phaser.CANVAS

/*game.global = {
    stage: "01",
    playSelection: [
        "Cat", "TankCat", "NULL", "NULL", "NULL"
    ]
}*/

//which stage you choose
var stage = 1;

var userRef;

//choose the cats to fight
var playSelection = [
    {
        number: 0
    },{
        number: 3
    },{
        number: 2
    },{
        number: 4
    },{
        number: 5
    }
]

//init data
var catsDataInit = [
    {
        name: "Cat",
        SPEED: -100,
        ATK: 10,
        HP: 50,
        cost: 50,
        animations_attack: [3, 4, 5, 6],
        animations_walk: [0, 1, 0, 2],
        animations_die: 7,
        AS: 1000,
        setSize: [46, 64, 0, 0],
        scaleX: 2,
        scaleY: 2
    },
    {
        name: "TankCat",
        SPEED: -100,
        ATK: 10,
        HP: 100,
        cost: 100,
        animations_attack: [3, 4, 5, 6],
        animations_walk: [0, 1, 0, 2],
        animations_die: 7,
        AS: 1000,
        setSize: [48, 98, 30, 0],
        scaleX: 2,
        scaleY: 2
    },
    {
        name: "AxeCat",
        SPEED: -100,
        ATK: 20,
        HP: 75,
        cost: 150,
        animations_attack: [3, 4, 5, 6],
        animations_walk: [0, 1, 0, 2],
        animations_die: 7,
        AS: 1200,
        setSize: [70, 100, 50, 27],
        scaleX: 1.4,
        scaleY: 1.4
    },
    {
        name: "BirdCat",
        SPEED: -150,
        ATK: 15,
        HP: 50,
        cost: 150,
        animations_attack: [4, 5, 6, 7, 8, 9],
        animations_walk: [0, 1, 2, 3],
        animations_die: 10,
        AS: 700,
        setSize: [133, 178, 60, 0],
        scaleX: 1.4,
        scaleY: 1.4
    },
    {
        name: "CowCat",
        SPEED: -150,
        ATK: 12,
        HP: 120,
        cost: 200,
        animations_attack: [0, 5, 6, 7],
        animations_walk: [1, 2, 3, 4],
        animations_die: 8,
        AS: 1000,
        setSize: [121, 124, 15, 0],
        scaleX: 1.7,
        scaleY: 1.7
    },
    {
        name: "WhaleCat",
        SPEED: -80,
        ATK: 50,
        HP: 200,
        cost: 350,
        animations_attack: [8, 9, 10, 11],
        animations_walk: [1, 2, 3, 4, 5, 6, 7],
        animations_die: 12,
        AS: 1200,
        setSize: [140, 210, 30, 0],
        scaleX: 1.7,
        scaleY: 1.7
    },
    {
        name: "GrossCat",
        SPEED: -100,
        ATK: 25,
        HP: 80,
        cost: 200,
        animations_attack: [5, 6, 7, 8],
        animations_walk: [0, 1, 2, 3, 4],
        animations_die: 9,
        AS: 1200,
        setSize: [121, 305, 121, 0],
        scaleX: 1.3,
        scaleY: 1.3
    },
    {
        name: "LizardCat",
        SPEED: -120,
        ATK: 30,
        HP: 100,
        cost: 250,
        animations_attack: [4, 5, 6, 7, 8, 9],
        animations_walk: [3, 2, 1, 0],
        animations_die: 10,
        AS: 1200,
        setSize: [100, 85, 118, 0],
        scaleX: 1.6,
        scaleY: 1.6
    },
    {
        name: "TitanCat",
        SPEED: -80,
        ATK: 100,
        HP: 250,
        cost: 400,
        animations_attack: [6, 7, 8, 9, 10, 11],
        animations_walk: [0, 1, 2, 3, 4, 5],
        animations_die: 12,
        AS: 1600,
        setSize: [115, 229, 100, 0],
        scaleX: 1.7,
        scaleY: 1.7
    }
]


//cats data such as ATK, HP
var catsData = [
    {
        name: "Cat",
        SPEED: -100,
        ATK: 10,
        HP: 50,
        cost: 50,
        animations_attack: [3, 4, 5, 6],
        animations_walk: [0, 1, 0, 2],
        animations_die: 7,
        AS: 1000,
        setSize: [46, 64, 0, 0],
        scaleX: 2,
        scaleY: 2
    },
    {
        name: "TankCat",
        SPEED: -100,
        ATK: 10,
        HP: 100,
        cost: 100,
        animations_attack: [3, 4, 5, 6],
        animations_walk: [0, 1, 0, 2],
        animations_die: 7,
        AS: 1000,
        setSize: [48, 98, 30, 0],
        scaleX: 2,
        scaleY: 2
    },
    {
        name: "AxeCat",
        SPEED: -100,
        ATK: 20,
        HP: 75,
        cost: 150,
        animations_attack: [3, 4, 5, 6],
        animations_walk: [0, 1, 0, 2],
        animations_die: 7,
        AS: 1200,
        setSize: [70, 100, 50, 27],
        scaleX: 1.4,
        scaleY: 1.4
    },
    {
        name: "BirdCat",
        SPEED: -150,
        ATK: 15,
        HP: 50,
        cost: 150,
        animations_attack: [4, 5, 6, 7, 8, 9],
        animations_walk: [0, 1, 2, 3],
        animations_die: 10,
        AS: 700,
        setSize: [133, 178, 60, 0],
        scaleX: 1.4,
        scaleY: 1.4
    },
    {
        name: "CowCat",
        SPEED: -150,
        ATK: 12,
        HP: 120,
        cost: 200,
        animations_attack: [0, 5, 6, 7],
        animations_walk: [1, 2, 3, 4],
        animations_die: 8,
        AS: 1000,
        setSize: [121, 124, 15, 0],
        scaleX: 1.7,
        scaleY: 1.7
    },
    {
        name: "WhaleCat",
        SPEED: -80,
        ATK: 50,
        HP: 200,
        cost: 1,
        animations_attack: [8, 9, 10, 11],
        animations_walk: [1, 2, 3, 4, 5, 6, 7],
        animations_die: 12,
        AS: 1200,
        setSize: [140, 210, 30, 0],
        scaleX: 1.7,
        scaleY: 1.7
    },
    {
        name: "GrossCat",
        SPEED: -100,
        ATK: 25,
        HP: 80,
        cost: 200,
        animations_attack: [5, 6, 7, 8],
        animations_walk: [0, 1, 2, 3, 4],
        animations_die: 9,
        AS: 1200,
        setSize: [121, 305, 121, 0],
        scaleX: 1.3,
        scaleY: 1.3
    },
    {
        name: "LizardCat",
        SPEED: -120,
        ATK: 30,
        HP: 100,
        cost: 250,
        animations_attack: [4, 5, 6, 7, 8, 9],
        animations_walk: [3, 2, 1, 0],
        animations_die: 10,
        AS: 1200,
        setSize: [100, 85, 118, 0],
        scaleX: 1.6,
        scaleY: 1.6
    },
    {
        name: "TitanCat",
        SPEED: -80,
        ATK: 100,
        HP: 250,
        cost: 400,
        animations_attack: [6, 7, 8, 9, 10, 11],
        animations_walk: [0, 1, 2, 3, 4, 5],
        animations_die: 12,
        AS: 1600,
        setSize: [115, 229, 100, 0],
        scaleX: 1.7,
        scaleY: 1.7
    }
    
]

//using these datas to determine the enemies
var stagesData = [
    {
        title: "stage01",
        bg: 'BattleBackgrounds01',
        type: 2,
        CD: 6000,
        enemySeleciton: [
            {
                name: "_Doge",
                number: 0
            },{
                name: "_Snache",
                number: 1
            }
        ]
    },
    {
        title: "stage02",
        bg: 'BattleBackgrounds01',
        type: 2,
        CD: 5000,
        enemySeleciton: [
            {
                name: "_Doge",
                number: 0
            },{
                name: "_Snache",
                number: 1
            }
        ]
    },
    {
        title: "stage03",
        bg: 'BattleBackgrounds02',
        type: 3,
        CD: 5000,
        enemySeleciton: [
            {
                name: "_Doge",
                number: 0
            },{
                name: "_Snache",
                number: 1
            },{
                name: "_ThoseGuys",
                number: 3
            }
        ]
    },
    {
        title: "stage04",
        bg: 'BattleBackgrounds02',
        type: 3,
        CD: 4000,
        enemySeleciton: [
            {
                name: "_Doge",
                number: 0
            },{
                name: "_Snache",
                number: 1
            },{
                name: "_ThoseGuys",
                number: 3
            }
        ]
    },
    {
        title: "stage05",
        bg: 'BattleBackgrounds02',
        type: 3,
        CD: 3500,
        enemySeleciton: [
            {
                name: "_Doge",
                number: 0
            },{
                name: "_Snache",
                number: 1
            },{
                name: "_ThoseGuys",
                number: 3
            }
        ]
    },{
        title: "stage06",
        bg: 'BattleBackgrounds03',
        type: 4,
        CD: 5000,
        enemySeleciton: [
            {
                name: "_Doge",
                number: 0
            },{
                name: "_Snache",
                number: 1
            },{
                name: "_TeacherBear",
                number: 2
            },{
                name: "_ThoseGuys",
                number: 3
            }
        ]
    },{
        title: "stage07",
        bg: 'BattleBackgrounds03',
        type: 4,
        CD: 4000,
        enemySeleciton: [
            {
                name: "_Doge",
                number: 0
            },{
                name: "_Snache",
                number: 1
            },{
                name: "_TeacherBear",
                number: 2
            },{
                name: "_ThoseGuys",
                number: 3
            }
        ]
    },{
        title: "stage08",
        bg: 'BattleBackgrounds04',
        type: 4,
        CD: 3000,
        enemySeleciton: [
            {
                name: "_Doge",
                number: 0
            },{
                name: "_Snache",
                number: 1
            },{
                name: "_TeacherBear",
                number: 2
            },{
                name: "_ThoseGuys",
                number: 3
            }
        ]
    },{
        title: "stage09",
        bg: 'BattleBackgrounds04',
        type: 4,
        CD: 2000,
        enemySeleciton: [
            {
                name: "_Doge",
                number: 0
            },{
                name: "_Snache",
                number: 1
            },{
                name: "_TeacherBear",
                number: 2
            },{
                name: "_ThoseGuys",
                number: 3
            }
        ]
    },{
        title: "stage10",
        bg: 'BattleBackgrounds04',
        type: 4,
        CD: 1000,
        enemySeleciton: [
            {
                name: "_Doge",
                number: 0
            },{
                name: "_Snache",
                number: 1
            },{
                name: "_TeacherBear",
                number: 2
            },{
                name: "_ThoseGuys",
                number: 3
            }
        ]
    }
]

var enemiesData = [
    {
        name: "Doge",
        SPEED: 100,
        ATK: 8,
        HP: 40,
        cost: 50,//maybe useful
        animations_attack: [3, 4, 5, 6],
        animations_walk: [0, 1, 0, 2, 0],
        animations_die: 7,
        AS: 1000,
        setSize: [50, 56, 0, 0],
        scaleX: 2,
        scaleY: 2
    },
    {
        name: "Snache",
        SPEED: 100,
        ATK: 10,
        HP: 50,
        cost: 50,//maybe useful
        animations_attack: [4, 5, 6, 7],
        animations_walk: [1, 2, 3],
        animations_die: 8,
        AS: 1000,
        setSize: [65, 65, 0, 0],
        scaleX: 2,
        scaleY: 2
    },
    {
        name: "TeacherBear",
        SPEED: 80,
        ATK: 30,
        HP: 250,
        cost: 150,//maybe useful
        animations_attack: [7, 8, 9, 10],
        animations_walk: [0, 1, 2, 3, 4, 5, 6],
        animations_die: 11,
        AS: 1200,
        setSize: [90, 200, 20, 11],
        scaleX: 1.5,
        scaleY: 1.5
    },
    {
        name: "ThoseGuys",
        SPEED: 150,
        ATK: 10,
        HP: 100,
        cost: 100,//maybe useful
        animations_attack: [5, 6, 7, 8, 9, 10, 11, 12, 13],
        animations_walk: [0, 1, 2, 3, 4],
        animations_die: 14,
        AS: 600,
        setSize: [60, 47, 0, 0],
        scaleX: 1.7,
        scaleY: 1.7
    }
]

game.state.add('boot', bootState);
game.state.add('load', loadState);
game.state.add('upgrade', upgradeState)
game.state.add('mainMenu', mainMenuState);
game.state.add('stage', stageState);
game.state.add('select', selectState);
game.state.add('play', playState);

game.state.start('boot');