var mainMenuState = {
    preload: function(){

    },
    create: function(){
        firebase.auth().onAuthStateChanged(async function (user) {
            if (user) {
                this.background = game.add.image(0, 0, 'MainMenuBackground');

                //getIntoStage
                this.MenuStartButton = game.add.image(18, 80, 'MenuStartButton');
                this.MenuStartButton.inputEnabled = true;
                this.MenuStartButton.events.onInputDown.add(function(){
                    game.state.start('stage');
                }, this);
        
                //getIntoUpgrade
                this.MenuUpgradeButton = game.add.image(18, 178, 'MenuUpgradeButton');
                this.MenuUpgradeButton.inputEnabled = true;
                this.MenuUpgradeButton.events.onInputDown.add(function(){
                    game.state.start('upgrade');
                }, this);
        
                //Logout
                this.ReturnButton = game.add.image(4, 577, 'ReturnButton');
                this.ReturnButton.inputEnabled = true;
                this.ReturnButton.events.onInputDown.add(function(){
                    firebase.auth().signOut().then(function() {
                        alert('Signed Out');
                        window.location.replace("signin.html");
                      })
                      .catch( function(error) {
                        alert('Signed Out Error');
                      });
                }, this);
                this.goldLabel = game.add.text(800, 3, '' + gold, {fontWeight: 'bold', font: '50px Agency FB ', fill:'#000000'});
            }
            else {
                window.location.replace("signin.html");
            }
            
        });
    },
    update: function(){

    }
}