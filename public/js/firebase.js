//firebase data
var user_email;
var level = 1;
var gold;
var key;
var key2;
var available;
//cat array small(available = 1)
var catArray = [];
var catArrayLevel = [];
//cat array big
var catArrayAll = [];
var catArrayLevelAll = [];

//use in upgrade
function getKey() {
    firebase.auth().onAuthStateChanged(function (user) {
        if (user) {
            user_email = user.email;
        }
    });
    
    firebase.database().ref('usersData').once('value')
    .then(function (snapshot) {
        snapshot.forEach(function(data){
        if(user_email == data.val().user) {
            key = data.key;
            loadData();
        }
        });
    })
    .catch(e => console.log(e.message));
}

function loadData() {
    firebase.database().ref(userRef + '/catList/Cat').once('value')
    .then(function (snapshot) {
        //get data
        available = snapshot.val().available;
        level = snapshot.val().level;
        //push all
        catArrayAll.push('Cat');
        catArrayLevelAll.push(level);
        //create small array
        if(available == 1) {
            catArray.push('Cat');
            catArrayLevel.push(level);
        }
    })

    firebase.database().ref(userRef + '/catList/TankCat').once('value')
    .then(function (snapshot) {
        available = snapshot.val().available;
        level = snapshot.val().level;
        catArrayAll.push('TankCat');
        catArrayLevelAll.push(level);
        if(available == 1) {
            catArray.push('TankCat');
            catArrayLevel.push(level);
        }
    })

    firebase.database().ref(userRef + '/catList/AxeCat').once('value')
    .then(function (snapshot) {
        available = snapshot.val().available;
        level = snapshot.val().level;
        catArrayAll.push('AxeCat');
        catArrayLevelAll.push(level);
        if(available == 1) {
            catArray.push('AxeCat');
            catArrayLevel.push(level);
        }
    })
    
    firebase.database().ref(userRef + '/catList/BirdCat').once('value')
    .then(function (snapshot) {
        available = snapshot.val().available;
        level = snapshot.val().level;
        catArrayAll.push('BirdCat');
        catArrayLevelAll.push(level);
        if(available == 1) {
            catArray.push('BirdCat');
            catArrayLevel.push(level);
        }
    })
    
    firebase.database().ref(userRef + '/catList/CowCat').once('value')
    .then(function (snapshot) {
        available = snapshot.val().available;
        level = snapshot.val().level;
        catArrayAll.push('CowCat');
        catArrayLevelAll.push(level);
        if(available == 1) {
            catArray.push('CowCat');
            catArrayLevel.push(level);
        }
    })
    
    firebase.database().ref(userRef + '/catList/WhaleCat').once('value')
    .then(function (snapshot) {
        available = snapshot.val().available;
        level = snapshot.val().level;
        catArrayAll.push('WhaleCat');
        catArrayLevelAll.push(level);
        if(available == 1) {
            catArray.push('WhaleCat');
            catArrayLevel.push(level);
        }
    })
    
    firebase.database().ref(userRef + '/catList/GrossCat').once('value')
    .then(function (snapshot) {
        available = snapshot.val().available;
        level = snapshot.val().level;
        catArrayAll.push('GrossCat');
        catArrayLevelAll.push(level);
        if(available == 1) {
            catArray.push('GrossCat');
            catArrayLevel.push(level);
        }
    })
    
    firebase.database().ref(userRef + '/catList/LizardCat').once('value')
    .then(function (snapshot) {
        available = snapshot.val().available;
        level = snapshot.val().level;
        catArrayAll.push('LizardCat');
        catArrayLevelAll.push(level);
        if(available == 1) {
            catArray.push('LizardCat');
            catArrayLevel.push(level);
        }
    })
    
        firebase.database().ref(userRef + '/catList/TitanCat').once('value')
        .then(function (snapshot) {
            available = snapshot.val().available;
            level = snapshot.val().level;
            catArrayAll.push('TitanCat');
            catArrayLevelAll.push(level);
            if(available == 1) {
                catArray.push('TitanCat');
                catArrayLevel.push(level);
            }
        })
    
    firebase.database().ref(userRef + '/money').once('value')
    .then(function (snapshot) {
        gold = snapshot.val().money;
    })
}

function getLevelKey() {
    for(var i=0;i<catArray.length;i++){
        var name = ''+catArray[i];
        var lv = ''+ catArrayLevel[i];
        firebase.database().ref(userRef+ '/catList/' + name).set({
            available : 1,
            level : lv
        })
    }
}   

function setLevel(tag,level) {
    firebase.database().ref(userRef + '/catList/' + tag + '/' + key2).set({
        level : level
    })
}

function getMoneyKey() {
    firebase.database().ref(userRef + '/money').set({
        money : gold
    })
}

//use in stage.js
function getKeytoStage() {
    firebase.auth().onAuthStateChanged(function (user) {
        if (user) {
            user_email = user.email;
        }
    });
    
    firebase.database().ref('usersData').once('value')
    .then(function (snapshot) {
        snapshot.forEach(function(data){
        if(user_email == data.val().user) {
            key = data.key;
            loadStage();
        }
        });
    })
    .catch(e => console.log(e.message));
}

/*function loadStage() {
    firebase.database().ref(userRef + '/stage').once('value')
    .then(function (snapshot) {
        firebase_stage = snapshot.val().stage;
        console.log(firebase_stage);
    })
}

function setStage() {
    firebase.database().ref(userRef + '/stage').set({
        stage : firebase_stage
    })
}*/

//select.js
function setSelectPlayer(team) {
    firebase.database().ref(userRef + '/SelectPlayer').set({
        SelectPlayer : team
    })
}