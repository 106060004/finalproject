function initApp() {
    // Login with Email/Password
    var txtEmail = document.getElementById('Email');
    var txtPassword = document.getElementById('Password');
    var btnLogin = document.getElementById('submitBtn');
    var btnSignUp = document.getElementById('addAccountBtn');
    var btnNewAccount = document.getElementById('newAccountBtn');
    var btnreturnLogin = document.getElementById('returnLoginBtn');

    var newtxtEmail = document.getElementById('newEmail');
    var newtxtPassword = document.getElementById('newPassword');


    btnLogin.addEventListener('click', function () {
        firebase.auth().signInWithEmailAndPassword(txtEmail.value, txtPassword.value)
        .then(function(){
            window.location.replace("index.html");
        })
        .catch(function(error) {
            // Handle Errors here.
            var errorCode = error.code;
            var errorMessage = error.message;
            create_alert("error", errorMessage);
            txtEmail.value = "";
            txtPassword.value = "";
        });
    });

    btnSignUp.addEventListener('click',async function () {
        var newtxtAccount = "";
        for(var i=0;newtxtEmail.value[i] != '@'; i++){
            newtxtAccount+=newtxtEmail.value[i];
        }
        userRef = 'usersData/' + newtxtAccount;
        console.log(userRef);
        await(firebase.auth().createUserWithEmailAndPassword(newtxtEmail.value, newtxtPassword.value)
        .then(function(){
            firebase.database().ref('usersData/' + newtxtAccount).set({
                user : newtxtAccount
              });
  
            createUser(newtxtAccount);
            newtxtEmail.value = "";
            newtxtPassword.value = "";
            var login = document.getElementById('login');
            var newaccount = document.getElementById('newaccount');
            $(login).css({"display" : "inline-block" });
            $(newaccount).css({"display" : "none" });
        })
        .catch(function(error) {
            var errorMessage = error.message;
            create_alert("error", errorMessage);
            newtxtEmail.value = "";
            newtxtPassword.value = "";
        }));
    });
    $(document).ready(function(){
        var login = document.getElementById('login');
        var newaccount = document.getElementById('newaccount');
        $(btnNewAccount).click(function(){
            $(login).css({"display" : "none" });
            $(newaccount).css({"display" : "inline-block" });
        })
        $(btnreturnLogin).click(function(){
            $(login).css({"display" : "inline-block" });
            $(newaccount).css({"display" : "none" });
        })
    })
}

function createUser(key) {
    firebase.database().ref('usersData/' + key + '/catList/Cat').set({
      available : 1,
      level : 1
    });
  
    firebase.database().ref('usersData/' + key + '/catList/TankCat').set({
      available : 1,
      level : 1
    });
  
    firebase.database().ref('usersData/' + key + '/catList/AxeCat').set({
      available : 1,
      level : 1
    });
  
    firebase.database().ref('usersData/' + key + '/catList/BirdCat').set({
      available : 1,
      level : 1
    });
  
    firebase.database().ref('usersData/' + key + '/catList/CowCat').set({
      available : 1,
      level : 1
    });
  
    firebase.database().ref('usersData/' + key + '/catList/WhaleCat').set({
      available : 0,
      level : 1
    });
  
    firebase.database().ref('usersData/' + key + '/catList/GrossCat').set({
      available : 0,
      level : 1
    });
  
    firebase.database().ref('usersData/' + key + '/catList/LizardCat').set({
      available : 0,
      level : 1
    });
  
    firebase.database().ref('usersData/' + key + '/catList/TitanCat').set({
      available : 1,
      level : 1
    });
  
    firebase.database().ref('usersData/' + key + '/talent/cat').set({
      HP : 0,
      Atk : 0,
      Speed : 0
    });
  
    firebase.database().ref('usersData/' + key + '/talent/tower').set({
      HP : 0,
      Atk : 0,
    });
  
    firebase.database().ref('usersData/' + key + '/stage').set({
      stage : 1
    });
    firebase.database().ref('usersData/' + key + '/money').set({
      money : 0
    });
  }

  // Custom alert
function create_alert(type, message) {
    if (type == "success") {
        alert("success");
    } else if (type == "error") {
        alert(message);
    }
}

window.onload = function () {
    initApp();
};