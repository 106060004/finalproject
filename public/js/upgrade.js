var upgradeState = {
    preload: function(){
        
    },
    create: function(){
        //background
        game.stage.backgroundColor = '#3498db';
        this.background = game.add.image(0, 0, 'Upgrade_background');
        this.background.scale.setTo(0.75, 0.89);
        //cat select -
        this.leftbtn = game.add.image(20, 190, 'Upgrade_left');
        this.leftbtn.inputEnabled = true;
        this.leftbtn.events.onInputDown.add(this.CatSelect_m1, this);
        game.add.tween(this.leftbtn).to({x:this.leftbtn.x-20, y:this.leftbtn.y}, 200).yoyo(true).loop().start();
        //cat select +
        this.rightbtn = game.add.image(game.width-100, 190, 'Upgrade_right');
        this.rightbtn.inputEnabled = true;
        this.rightbtn.events.onInputDown.add(this.CatSelect_p1, this);
        game.add.tween(this.rightbtn).to({x:this.rightbtn.x+20, y:this.rightbtn.y}, 200).yoyo(true).loop().start();
        //go back
        this.backbtn = game.add.image(10, game.height-110, 'Upgrade_back');
        this.backbtn.scale.setTo(0.8, 0.8);
        this.backbtn.inputEnabled = true;
        this.backbtn.events.onInputDown.add(this.StateBack, this);
        //upgrade cat
        this.upgradebtn = game.add.image(10, 400, 'Upgrade_upgrade');
        this.upgradebtn.scale.setTo(0.8, 0.7);
        this.upgradebtn.inputEnabled = true;
        this.upgradebtn.events.onInputDown.add(this.CatUpgrade, this);
        this.upgradebtn.alpha = 0.7;
        this.upgradebtn.events.onInputOver.add(()=>{this.upgradebtn.alpha = 1;}, this);
        this.upgradebtn.events.onInputOut.add(()=>{this.upgradebtn.alpha = 0.7;}, this);
        //firebase
        getKey();
        //cat display
        this.CatIndex = 0;
        this.Cat = game.add.image(game.width/2, 250, 'Upgrade_Cat');
        this.Cat.anchor.setTo(0.5, 0.5);
        this.Cat.scale.setTo(0.8, 0.8);
        this.LeftCat = game.add.image(game.width/2-300, 250, 'Upgrade_Cat');
        this.LeftCat.anchor.setTo(0.5, 0.5);
        this.LeftCat.scale.setTo(0.6, 0.6);
        this.LeftCat.alpha = 0;
        this.RightCat = game.add.image(game.width/2+300, 250, 'Upgrade_TankCat');
        this.RightCat.anchor.setTo(0.5, 0.5);
        this.RightCat.scale.setTo(0.6, 0.6);
        this.RightCat.alpha = 0.6;
        //money text
        this.goldLabel = game.add.text(800, 3, this.money, {fontWeight: 'bold', font: '50px Agency FB ', fill:'#000000'});
        //level text
        this.level = 1;
        this.levelLabel = game.add.text(615, 235, this.level, {fontWeight: 'bold', font: '40px Agency FB', fill:'#ffff6e'});
        //cost text
        this.cost = 1000;
        this.costLabel = game.add.text(535, 295, this.cost, {fontWeight: 'bold', font: '40px Agency FB', fill:'#12ffff'});
        //say something
        this.sth = '貓咪貓咪喵咪';
        this.saysthLabel = game.add.text(game.width/2, 560, this.sth, {fontWeight: 'bold', font: '40px Agency FB', fill:'#ffffff'});
        this.saysthLabel.anchor.setTo(0.5, 0.5);
    },
    update: function(){
        //console.log(catArray.length);
        this.level = catArrayLevel[this.CatIndex];
        if(this.level >= 10){
            this.cost = 'lv MAX';
        }else{
            this.cost = this.level *1000;
        }
        this.money = gold;
        this.goldLabel.text = '' + this.money;
        this.levelLabel.text = '' + this.level;
        this.costLabel.text = '' + this.cost;
    },
    CatSelect_m1: function(){
        if(this.CatIndex){
            this.CatIndex -= 1;
            //kill image
            this.Cat.kill();
            this.LeftCat.kill();
            this.RightCat.kill();
            //redraw
            this.MenuRedraw();
        }
    },
    CatSelect_p1: function(){
        if(this.CatIndex < catArray.length-1){
            this.CatIndex += 1
            //kill image
            this.Cat.kill();
            this.LeftCat.kill();
            this.RightCat.kill();
            //redraw
            this.MenuRedraw();
        };
    },
    CatUpgrade: function(){
        if(gold >= this.cost && this.level < 10) {
            gold -= this.cost;
            catArrayLevel[this.CatIndex]++;
        }
    },
    StateBack: function(){
        getLevelKey();
        getMoneyKey();
        game.state.start('mainMenu');
    },
    MenuRedraw: function(){
        if(catArray[this.CatIndex]){
            this.Cat = game.add.image(game.width/2, 250, 'Upgrade_'+catArray[this.CatIndex]);
            this.Cat.anchor.setTo(0.5, 0.5);
            this.Cat.scale.setTo(0.8, 0.8);
            this.levelLabel.bringToTop();
            this.costLabel.bringToTop();
        }   
        if(catArray[this.CatIndex-1]){
            this.LeftCat = game.add.image(game.width/2-300, 250, 'Upgrade_'+catArray[this.CatIndex-1]);
            this.LeftCat.anchor.setTo(0.5, 0.5);
            this.LeftCat.scale.setTo(0.6, 0.6);
            this.LeftCat.alpha = 0.6;
        }
        if(catArray[this.CatIndex+1]){
            this.RightCat = game.add.image(game.width/2+300, 250, 'Upgrade_'+catArray[this.CatIndex+1]);
            this.RightCat.anchor.setTo(0.5, 0.5);
            this.RightCat.scale.setTo(0.6, 0.6);
            this.RightCat.alpha = 0.6; 
        }
    }
};